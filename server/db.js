//import Sequelize from 'sequelize';
//import Faker from 'faker';
//import _ from 'lodash';
const Sequelize = require('sequelize');
const Faker = require('faker');
const _ = require('lodash');

const Conn = new Sequelize(

    'konquest', //db name
    'ragnarok', // user
    'ultima12', // pw
    {
        dialect: 'postgres',
        host: 'postgres.ddns.net'
    }
);


const User = Conn.define('user', {
    regNum: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    firstName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    lastName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        validate: {
            isEmail: true
        }
    },
    charClass: {
        type: Sequelize.STRING,
        allowNull: false
    },
    xp: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    feets: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    attendingNextYear: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },

});

const Post = Conn.define('post', {
    title: {
        type: Sequelize.STRING,
        allowNull: false
    },
    content: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

const Perk = Conn.define('perk', {
    title: {
        type: Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

const Customization = Conn.define('customization', {
    title: {
        type: Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

// Relations
User.hasMany(Post);
User.hasMany(Perk);
User.hasMany(Customization);
Post.belongsTo(User);
Perk.belongsTo(User);
Customization.belongsTo(User);


// Conn.sync();

// Testing with fake users
Conn.sync({force: true}).then(() => {
    _.times(5, () => {
        return User.create({
            password: Faker.internet.userName(),
            regNum: Faker.random.number(),
            email: Faker.internet.email(),
            firstName: Faker.name.firstName(),
            lastName: Faker.name.lastName(),
            charClass: Faker.name.jobTitle(),
            xp: Faker.random.number(),
            feets: Faker.random.number(),
            attendingNextYear: Faker.random.boolean(),

        }).then((user) => {

            user.createPost({
                title: `Sample post by ${user.firstName}`,
                content: 'here is some content'
            });

            user.createPerk({
                title: `Sample perk for ${user.firstName}`,
                description: 'here is a description'
            });
            user.createCustomization({
                title: `Sample customization for ${user.firstName}`,
                description: 'here is a description'
            });
            return user;
        })


    });
});

module.exports = Conn;
//export default Conn;